#include <cstdio>
#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

#if VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_TBB
#include <tbb/task_scheduler_init.h>
#endif

#include <vtkm/cont/Timer.h>
#include <vtkm/cont/testing/MakeTestDataSet.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/worklet/DispatcherMapField.h>

#if (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_SERIAL)
#pragma message ("Using Serial")
#elif (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_TBB)
#pragma message ("Using TBB")
#elif (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_CUDA)
#pragma message ("Using CUDA")
#else
#pragma message ("Device Unknown")
#endif

using namespace std;
//using namespace boost::numeric;

bool bShowGrids = false;
#ifdef PROFILING
bool bShowOriginal = false;
#else
bool bShowOriginal = true;
#endif
bool bShowOutput = false;
bool bUseVTKSimplify = false;

double origin[3];
int xdim, ydim, zdim;
void set_origin_and_dim();
double grid_width = 0.015625;
//float one_over_grid_width = 1./grid_width;

int grid_division = 30;



int input_len = 1000000;
int unique_keys = 1000;

vector<vtkm::Id> data;

void gen_data()
{
  data = vector<vtkm::Id>(input_len);
  vtkm::Id i;
  for (i=0; i< input_len; i++)
    if (RAND_MAX <= (1L<<15))
      data[i] = (rand() & ((1L<<15)-1)) | (static_cast<vtkm::Id>(rand()) << 15); // 1<<15 = 32768 = RAND_MAX
    else
      data[i] = rand();
  cout << "input size: " << data.size() << endl;
}

// input: points  output: cid of the points
class MapPointsWorklet : public vtkm::worklet::WorkletMapField {
private:
  //const VTKM_EXEC_CONSTANT_EXPORT GridInfo<PointType> Grid;
  int MOD;
public:
  typedef void ControlSignature(FieldIn<> , FieldOut<>);
  typedef void ExecutionSignature(_1, _2);

  VTKM_CONT_EXPORT
  MapPointsWorklet(int unique_keys)
    : MOD(unique_keys)
  { }

  VTKM_EXEC_EXPORT
  void operator()(const vtkm::Id id, vtkm::Id &cid) const
  {
    cid = id % MOD;
  }
};


void run(bool show)
{
  vtkm::cont::ArrayHandle<vtkm::Id> vtkmData = vtkm::cont::make_ArrayHandle<vtkm::Id>(data);
  vtkm::cont::ArrayHandle<vtkm::Id> vtkmDataMOD;

  vtkm::cont::Timer<> timer;

  vtkm::worklet::DispatcherMapField<MapPointsWorklet>(MapPointsWorklet(unique_keys))
      .Invoke(vtkmData, vtkmDataMOD);

  if (show)
    cout << "Map time: " << timer.GetElapsedTime() << endl;
  timer.Reset();


  vtkm::cont::DeviceAdapterAlgorithm<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>::Sort(vtkmDataMOD);

  if (show)
    cout << "Sort time: " << timer.GetElapsedTime() << endl;
  timer.Reset();

  vtkm::cont::DeviceAdapterAlgorithm<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>::Unique(vtkmDataMOD);

  if (show) {
    cout << "Unique time: " << timer.GetElapsedTime() << endl;
    cout << "Size after unique: " << vtkmDataMOD.GetNumberOfValues() << endl;
    //for (int i=0; i< vtkmDataMOD.GetNumberOfValues(); i++)
    //  cout << vtkmDataMOD.GetPortalConstControl().Get(i) << " " ;
  }
}

void benchmark()
{
  int c;
  const int tests = 3;
  double t1=0;

  for (c=0; c<tests+1; c++){
    if (c>0)
      run(true);
    else
      run(false);
    cout << "----" << endl;
  }
}

int main( int argc, char **argv )
{
    cout << "Input arguments: <input length=1000000> <unique keys=1000> <max threads>\n";
#if VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_TBB
    int cores = tbb::task_scheduler_init::default_num_threads();
    if (argc>3)
        cores = atoi(argv[3]);
    tbb::task_scheduler_init init(cores);  //!!!!!!!!!!!!!!!!
    cout << "Max threads to use: "  << cores << endl;
#endif
    if (argc>2)
        unique_keys = atoi(argv[2]);

    if (argc>1)
      input_len = atoi(argv[1]);

    gen_data();
    benchmark();

    return 0;

}
